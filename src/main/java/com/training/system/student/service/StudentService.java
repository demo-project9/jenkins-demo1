package com.training.system.student.service;

import java.io.IOException;
import java.util.List;

import com.training.system.student.exception.StudentNotFoundException;
import com.training.system.student.model.Student;

public interface StudentService {

	public List<Student> retrieveAllStudents() throws ClassNotFoundException, IOException;
	
	public int addStudent(Student student) throws IOException, ClassNotFoundException;
	
	public Student findById(int studentId) throws ClassNotFoundException, IOException, StudentNotFoundException;
	
	public boolean deleteStudent(int studentId) throws ClassNotFoundException, IOException;

	public Student updateStudent(int studentId, Student studentDetails) throws ClassNotFoundException, IOException;
}
