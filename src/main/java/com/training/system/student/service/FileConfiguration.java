package com.training.system.student.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.training.system.student.model.Student;

public class FileConfiguration {

	private static ObjectMapper objectMapper = new ObjectMapper();

	public static void writeStudentDataToFile(List<Student> studentDaoList)
			throws JsonGenerationException, JsonMappingException, IOException {
		objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
		objectMapper.writeValue(new File("StudentDetails.json"), studentDaoList);
//		System.out.println(student);
	}

	public static List<Student> readStudentDataFromFile() throws JsonParseException, JsonMappingException, IOException {
		List<Student> studentDaoList = new ArrayList<Student>();
		TypeReference<List<Student>> reference = new TypeReference<List<Student>>() {
		};
		studentDaoList = objectMapper.readValue(new File("StudentDetails.json"), reference);
		return studentDaoList;
	}
}
