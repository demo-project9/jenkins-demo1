package com.training.system.student.service;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.stereotype.Service;

import com.training.system.student.exception.StudentNotFoundException;
import com.training.system.student.model.Student;

@Service
public class StudentServiceImpl implements StudentService {

	private List<Student> studentList;

	@Override
	public List<Student> retrieveAllStudents() throws ClassNotFoundException, IOException {
		studentList = FileConfiguration.readStudentDataFromFile();
		return studentList;
	}

	@Override
	public int addStudent(Student studentDetails) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		studentList = FileConfiguration.readStudentDataFromFile();
		int recordExists = (int) studentList.stream().filter(stud -> stud.getId() == studentDetails.getId()).count();
		if (recordExists == 0) {
			studentList.add(studentDetails);
	 		FileConfiguration.writeStudentDataToFile(studentList);
		}
		return recordExists;

	}

	@Override
	public Student findById(int studentId) throws ClassNotFoundException, IOException, StudentNotFoundException {
		studentList = FileConfiguration.readStudentDataFromFile();
		try {
			return studentList.stream().filter(stud -> stud.getId() == studentId).findFirst().get();
		} catch (NoSuchElementException exp) {
			throw new StudentNotFoundException("Student record with student ID : " + studentId + " does not exists.");
		}
	}

	@Override
	public boolean deleteStudent(int studentId) throws ClassNotFoundException, IOException {

		studentList = FileConfiguration.readStudentDataFromFile();

		// TODO Auto-generated method stub
		for (int i = 0; i < studentList.size(); i++) {
			Student student = studentList.get(i);
			if (student.getId() == studentId) {
				studentList.remove(i);
				FileConfiguration.writeStudentDataToFile(studentList);
				return true;
			}
		}

		return false;
	}

	@Override
	public Student updateStudent(int studentId, Student studentDetails)
			throws ClassNotFoundException, IOException {
		studentList = FileConfiguration.readStudentDataFromFile();
		// TODO Auto-generated method stub
		for (int i = 0; i < studentList.size(); i++) {
			Student student = studentList.get(i);
			if (student.getId() == studentId) {
				studentList.set(i, studentDetails);
				FileConfiguration.writeStudentDataToFile(studentList);
				return studentList.get(i);
			}
		}

		return null;

	}
}
