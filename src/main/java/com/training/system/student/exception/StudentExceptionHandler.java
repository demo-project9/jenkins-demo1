package com.training.system.student.exception;

import java.time.LocalDate;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class StudentExceptionHandler {

	@ExceptionHandler(StudentNotFoundException.class)
	public ResponseEntity<ErrorDetails> handleStudentNotFoundException(Exception exp, WebRequest request) {
		ErrorDetails details = new ErrorDetails(LocalDate.now(), exp.getMessage(), request.getDescription(false));
		return new ResponseEntity<ErrorDetails>(details, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(StudentAlreadyExistsException.class)
	public ResponseEntity<ErrorDetails> handleStudentAlreadyExists(Exception exp, WebRequest request) {
		ErrorDetails details = new ErrorDetails(LocalDate.now(), exp.getMessage(), request.getDescription(false));
		return new ResponseEntity<ErrorDetails>(details, HttpStatus.BAD_REQUEST);
	}

}
