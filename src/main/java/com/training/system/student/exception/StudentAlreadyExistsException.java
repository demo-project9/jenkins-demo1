package com.training.system.student.exception;

public class StudentAlreadyExistsException extends Exception{

	public StudentAlreadyExistsException(String message) {
		super(message);
	}
}
