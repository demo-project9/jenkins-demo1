package com.training.system.student.exception;

public class StudentNotFoundException extends Exception {

	public StudentNotFoundException(String message) {
		super(message);
	}
}
