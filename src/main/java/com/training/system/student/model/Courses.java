package com.training.system.student.model;

import org.springframework.stereotype.Component;

@Component
public class Courses {

	private String Subject1;
	private String Subject2;
	private String Subject3;

	public Courses() {

	}

	public Courses(String subject1, String subject2, String subject3) {
		super();
		Subject1 = subject1;
		Subject2 = subject2;
		Subject3 = subject3;
	}

	public String getSubject1() {
		return Subject1;
	}

	public void setSubject1(String subject1) {
		Subject1 = subject1;
	}

	public String getSubject2() {
		return Subject2;
	}

	public void setSubject2(String subject2) {
		Subject2 = subject2;
	}

	public String getSubject3() {
		return Subject3;
	}

	public void setSubject3(String subject3) {
		Subject3 = subject3;
	}

}
