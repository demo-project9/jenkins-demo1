package com.training.system.student.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.system.student.exception.StudentAlreadyExistsException;
import com.training.system.student.exception.StudentNotFoundException;
import com.training.system.student.model.Student;
import com.training.system.student.service.StudentService;

@RestController
@RequestMapping("/student")
public class StudentController {

	@Autowired
	private StudentService studentService;

	@GetMapping("/retrieveAllStudentDetails")
	public ResponseEntity<List<Student>> retrieveAllStudentDetails() throws ClassNotFoundException, IOException {
		List<Student> studentList = studentService.retrieveAllStudents();
		return new ResponseEntity<List<Student>>(studentList, HttpStatus.OK);
	}

	@PostMapping("/addStudent")
	public ResponseEntity<String> createStudent(@RequestBody Student studentDetails)
			throws ClassNotFoundException, IOException, StudentAlreadyExistsException {
		int recordExits = studentService.addStudent(studentDetails);
		if (recordExits != 0) {
			throw new StudentAlreadyExistsException(
					"Student with student ID : " + studentDetails.getId() + " already exists.");
		} else {
			return new ResponseEntity<String>("Student record added successfully", HttpStatus.CREATED);
		}
	}

	@GetMapping("/retrieveStudent/{studentId}")
	public ResponseEntity<Student> getStudentById(@PathVariable int studentId)
			throws ClassNotFoundException, IOException, StudentNotFoundException {
		Student student = studentService.findById(studentId);
		return new ResponseEntity<Student>(student, HttpStatus.OK);
	}

	@PutMapping("/updateStudent/{studentId}")
	public ResponseEntity<Student> updateStudent(@PathVariable int studentId, @RequestBody Student studentDetails)
			throws ClassNotFoundException, IOException, StudentNotFoundException {
		Student student = studentService.updateStudent(studentId, studentDetails);
		if (student == null) {
			throw new StudentNotFoundException("Student record with student ID : " + studentId + " does not exists.");
		} else {
			return new ResponseEntity<Student>(student, HttpStatus.OK);
		}
	}

	@DeleteMapping("/deleteStudent/{id}")
	public ResponseEntity<String> deleteStudent(@PathVariable(value = "id") int studentId)
			throws ClassNotFoundException, IOException, StudentNotFoundException {
		if (studentService.deleteStudent(studentId)) {
			return new ResponseEntity<String>(
					"Student record with student ID : " + studentId + " deleted successfully.", HttpStatus.OK);
		} else {
			throw new StudentNotFoundException("Student record with student ID : " + studentId + " does not exists.");
		}
	}
}
