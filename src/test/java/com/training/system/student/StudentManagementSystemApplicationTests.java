package com.training.system.student;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.training.system.student.exception.StudentNotFoundException;
import com.training.system.student.model.Courses;
import com.training.system.student.model.Student;
import com.training.system.student.service.StudentService;
import com.training.system.student.service.StudentServiceImpl;

class StudentManagementSystemApplicationTests {

	StudentService studentService = new StudentServiceImpl();

	Student student;

	private List<Courses> courses = new ArrayList<>();

	@BeforeEach
	void setUp() throws JsonParseException, JsonMappingException, IOException, ClassNotFoundException {
		Courses c1 = new Courses("BI", "ML", "ICS");
		Courses c2 = new Courses("DA", "HPC", "ICS");
		courses.add(c1);
		courses.add(c2);
		student = new Student(110, "Rohan", "Pune", "rohan@gmail.com", "Computer", "B+", 123456, "Modern", "B12354",
				courses);
		studentService.addStudent(student);
	}

	@Test
	void addStudentRecordTest() throws ClassNotFoundException, IOException {
		int recordExists = studentService.addStudent(student);
		assertEquals(1, recordExists);
	}

	@Test
	void retrieveStudentRecordTest() throws ClassNotFoundException, IOException, StudentNotFoundException {
		Student actualStudentRecord = studentService.findById(110);
		assertEquals(student.getId(), actualStudentRecord.getId());
	}

	@Test
	void updateStudentRecordTest() throws ClassNotFoundException, IOException, StudentNotFoundException {
		Student newStudentRecord = new Student(110, "Sarika", "Pune", "sarikan@gmail.com", "Computer", "B+", 123456,
				"Modern", "B12354", courses);
		Student actualStudentRecord = studentService.updateStudent(110, newStudentRecord);
		assertEquals(newStudentRecord, actualStudentRecord);
	}

	@Test
	void deleteStudentRecordTest() throws ClassNotFoundException, IOException, StudentNotFoundException {
		boolean actual = studentService.deleteStudent(110);
		assertTrue(actual);
	}

	@Test
	void findStudentRecordWhichDoesNotExists() {
		assertThrows(StudentNotFoundException.class, () -> studentService.findById(1020));
	}

	@Test
	void updateStudentRecordWhichDoesNotExists() throws ClassNotFoundException, IOException {
		Student newStudentRecord = new Student(110, "Sarika", "Pune", "sarikan@gmail.com", "Computer", "B+", 123456,
				"Modern", "B12354", courses);
		assertNull(studentService.updateStudent(10201, newStudentRecord));
	}

}
 